import logging
from os import environ
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
import redis
import json
from configparser import ConfigParser
import sys

sys.path.append('..')

from utilities.logging import get_logger

app = FastAPI()

# read config file
config_parser = ConfigParser()
config_parser.read('../conf/main.conf')
module_conf = config_parser["MODULE"]

# init logging
logger_name = environ.get('api_get_logger', 'api')
# for local logs storage and stdout use DEBUG_FLAG = 1
DEBUG_FLAG = environ.get('api_DEBUG_FLAG', 1)
log_level = environ.get('api_log_level', 'INFO')

logger = get_logger(logger_name, DEBUG_FLAG, log_level)

# get redis settings
DB_REDIS_PASSWORD = environ.get('redis_pass', module_conf.get('redis_pass'))
DB_REDIS_HOST = environ.get('redis_host', module_conf.get('redis_host'))
DB_REDIS_PORT = environ.get('redis_port', module_conf.get('redis_port'))

API_SERVER_HOST = environ.get('api_server_host', module_conf.get('api_server_host'))
API_SERVER_PORT = environ.get('api_server_port', module_conf.get('api_server_port'))

CORS_HOST = environ.get('CORS_HOST', module_conf.get('CORS_HOST')).split(",")

# CORS
origins = CORS_HOST

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class RedisManager:
    def __init__(self):
        try:
            self.pool_urls = redis.ConnectionPool(host=DB_REDIS_HOST, port=int(DB_REDIS_PORT), db=0,
                                                  decode_responses=True, password=DB_REDIS_PASSWORD)
            self.urls_connect = redis.Redis(connection_pool=self.pool_urls)
        except Exception as ex:
            logging.warning(f"Database exception: {ex}")

    async def get_data(self):
        data_list = []

        try:
            stream_name = "football_uaxbet"
            resp = self.urls_connect.xrevrange(stream_name, min='-', max='+', count=1)
            data_list = json.loads(resp[0][1]['data_list'].replace("'", '"'))
        except Exception as e:
            logging.warning(f"Database exception: {e}")
            return [{"Data exception": f"{e}"}]

        return data_list


redis_manager = RedisManager()


@app.get('/events')
async def sport():
    data_list = await redis_manager.get_data()
    return data_list


if __name__ == "__main__":
    uvicorn.run("main:app", port=int(API_SERVER_PORT), host=API_SERVER_HOST, reload=True)
