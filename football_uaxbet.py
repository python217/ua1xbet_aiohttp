import asyncio
import aiohttp
import logging
from os import environ
from bs4 import BeautifulSoup
from utilities.logging import get_logger
import redis
from datetime import datetime
from configparser import ConfigParser


# read config file
config_parser = ConfigParser()
config_parser.read('conf/main.conf')
module_conf = config_parser["MODULE"]

# init logging
logger_name = environ.get('football_uaxbet_get_logger', 'football_uaxbet')
# for local logs storage and stdout use DEBUG_FLAG = 1
DEBUG_FLAG = environ.get('football_uaxbet_DEBUG_FLAG', 1)
log_level = environ.get('football_uaxbet_log_level', 'INFO')

logger = get_logger(logger_name, DEBUG_FLAG, log_level)

# get redis settings
DB_REDIS_HOST = environ.get('redis_host', module_conf.get('redis_host'))
DB_REDIS_PORT = environ.get('redis_port', module_conf.get('redis_port'))


async def add_data_redis(data_key, data_value):
    try:
        pool = redis.ConnectionPool(host=DB_REDIS_HOST, port=DB_REDIS_PORT, db=0, decode_responses=True)
        r = redis.Redis(connection_pool=pool)
        r.xadd(data_key, {"data_list": f"{data_value}"})
        logging.info('Redis info: OK')
    except Exception as redis_error:
        logging.warning(f"Redis error: {redis_error}")


async def collect_data(teams, score, bets, keys):
    markets = []
    ret_lst = []

    for index in range(len(bets)):
        reg_outcomes_lst = []
        both_outcomes_lst = []
        # outcomes
        for key, value in zip(keys, bets[index]):
            outcomes_dict = {
                "active": ["False" if value == '-' else "True"][0],
                "odds": f"{value}",
                "type": f"{key}"
            }
            if key == '1' or key == '2' or key == 'X':
                reg_outcomes_lst.append(outcomes_dict)
            else:
                both_outcomes_lst.append(outcomes_dict)

        markets_reg_dict = {
            "title": "1X2 Regular time",
            "outcomes": reg_outcomes_lst
        }

        markets_both_dict = {
            "title": "Both Teams To Score",
            "outcomes": both_outcomes_lst
        }

        markets.append([markets_reg_dict, markets_both_dict])

    for j in range(len(teams)):
        try:
            football_data_scrape = {
                "away": f"{teams[j][1]}",
                "home": f"{teams[j][0]}",
                "currentScore": f"{[i[:len(i) // 2][0] for i in score][0]} : {[i[len(i) // 2:][0] for i in score][0]}",
                "markets": markets[j]
            }
        except IndexError as e:
            football_data_scrape = {
                "away": f"{teams[j][1]}",
                "home": f"{teams[j][0]}",
                "currentScore": "0 : 0",
                "markets": markets[j]
            }
            pass
        except Exception as e:
            logging.warning(e)

        ret_lst.append(football_data_scrape)
    return ret_lst


async def scrape_data(rows):
    teams_f = []
    scores_f = []
    bets_f = []
    # Teams
    teams = rows.findAll("span", class_="c-events__teams")
    for team in teams:
        teams_f.append(team.text.split('\n'))
    for index, items in enumerate(teams_f):
        teams_f[index] = list(filter(lambda x: x != '', items))
    # Score
    scores = rows.findAll("div", class_="c-events-scoreboard__lines")
    for score in scores:
        scores_f.append(score.text.split("\n"))
    for index, items in enumerate(scores_f):
        scores_f[index] = list(filter(lambda x: x != '', items))
    # Bets
    bets = rows.findAll("div", class_="c-bets")
    for bet in bets:
        bets_f.append(bet.text.split("\n"))
    for index, items in enumerate(bets_f):
        bets_f[index] = list(filter(lambda x: x != '', items))
    temp = []
    key_lst = bets_f[0]
    for item in bets_f:
        if key_lst != item:
            temp.append(item)
            # temp.append(dict(zip(key_lst, value)))
    bets_f = temp

    return teams_f, scores_f, bets_f, key_lst


async def scrape(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            body = await resp.text()
            soup = BeautifulSoup(body, 'html.parser')
            rows = soup.find("div", class_="dashboard c-events greenBack")
            teams, score, bets, key = await scrape_data(rows)
            complete_data = await collect_data(teams=teams, score=score, bets=bets, keys=key)
            return complete_data


async def main():
    url = "https://ua1xbet.com/us/live/football"
    data = await scrape(url)
    timestamp = datetime.timestamp(datetime.now())
    rec_status = await add_data_redis(data_key=f"football_uaxbet", data_value=data)
    logging.info("Redis INFO: " + rec_status)


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
